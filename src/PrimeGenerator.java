public class PrimeGenerator {

    int nPrimeGeneratedNumber;

    /*
    Constructor for PrimeGenerator class
     */
    public PrimeGenerator(int mEnteredNumber){
        nPrimeGeneratedNumber = mEnteredNumber;
    }



    public int nextPrime(int mNumberChecker){
        //While the number being sent to isPrime returns false
        //increment the number and check again
        while (!isPrime(mNumberChecker)) {
            mNumberChecker++; //increment to keep checking for a prime number
        }
        return mNumberChecker; //return the next prime from the entered number unless the number is prime
    }


    /*
    Check if number entering into isPrime is prime
    if so return true
     */
        public boolean isPrime(int mNumberIsPrime) {
            if(mNumberIsPrime == 0){
                return false;
            } else if(mNumberIsPrime == 1){
                return false;
            }

            /*
            loop up to number
            check if number is divisible by any number other than one up to itself
            if it is return false
            otherwise it is prime and return true
             */
            for (int nNumberDivisor = 2; nNumberDivisor < mNumberIsPrime; nNumberDivisor++) {

                //while the remainder of the number divided by any number from 2 up to itself is 0
                //return false for the number is not prime
                if (mNumberIsPrime % nNumberDivisor == 0) {
                    return false;
                }
            }
                return true; //return true otherwise for the number is prime
        }
}
