import java.util.*;

/*
 *Austin Rafter
 * 013433901
 * CS 049J
 * 9/19/2020
 *
 * Take user input as a positive integer
 * Print all prime numbers leading up to and including that integer
 *
 */

public class P6_5 {

    public static void main(String[] args) {
        System.out.println("Enter a positive integer: "); //prompt for number
        Scanner scanObject = new Scanner(System.in);
        int nEnteredNumber = scanObject.nextInt(); //user enters an int

        //Check that user has entered a positive integer
        //Have them re enter if negative
        if(nEnteredNumber < 0 ){
            System.out.println("Please enter a positive integer");
            nEnteredNumber = scanObject.nextInt(); //user enters an int
        }

        //Initiate constructor
        PrimeGenerator primeNumberChecker = new PrimeGenerator(nEnteredNumber);



        int nNumberCheck = primeNumberChecker.nextPrime(1); //initial number to check for duplicates

        /*loop from 1 to the entered number calling nextPrime on the object at each iteration
        printing the prime number output
         */
        for( int nNumberChecker = 1; nNumberChecker <= (nEnteredNumber); nNumberChecker++){
            if(nNumberChecker == 1){
                continue; //skip 1 since it is not considered prime
            }

            /*
            Check if number is 2 which is prime
             */
            if(primeNumberChecker.nextPrime(nNumberChecker) == 2){
                System.out.print(primeNumberChecker.nextPrime(nNumberChecker)+ " ");
            }

            /*
            check that number being printed is greater than the previous  so
            as not to print duplicates
             */
            if ( primeNumberChecker.nextPrime(nNumberChecker) > nNumberCheck) {
                //Check that prime numbers are all under the entered number
                if(primeNumberChecker.nextPrime(nNumberChecker) <= nEnteredNumber) {
                    nNumberCheck = primeNumberChecker.nextPrime(nNumberChecker); //reset the check with the iteration number
                    System.out.print(primeNumberChecker.nextPrime(nNumberChecker) + " "); //print the number
                }

               }

        }


    }


}

